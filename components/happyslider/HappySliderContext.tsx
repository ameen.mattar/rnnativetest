import React, {useContext, useState} from 'react';

interface HappySliderContextType {
  isSliding: boolean;
  setIsSliding: (isSliding: boolean) => void;
}

const HappySliderContext = React.createContext<HappySliderContextType>({
  isSliding: false,
  setIsSliding: (_isSliding: boolean) => {},
});

type Props = {
  children: JSX.Element;
};

export const HappySliderContextProvider: React.FC<Props> = ({children}) => {
  let [isSliding, setIsSliding] = useState(false);
  return (
    <HappySliderContext.Provider value={{isSliding, setIsSliding}}>
      {children}
    </HappySliderContext.Provider>
  );
};

export function useHappySliderContext() {
  return useContext(HappySliderContext);
}

type ContextInjectorFn = (
  isSliding: boolean,
  setIsSliding: (isSliding: boolean) => void,
) => JSX.Element;

type ContextInjectorProps = {
  render: ContextInjectorFn;
};

export function HappySliderContextInjector(props: ContextInjectorProps) {
  const {isSliding, setIsSliding} = useHappySliderContext();
  return props.render(isSliding, setIsSliding);
}
