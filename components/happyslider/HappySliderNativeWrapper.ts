import {requireNativeComponent} from 'react-native';

export const NATIVE_REACT_CLASS = 'CPLHappySlider';

const CPLHappySlider = requireNativeComponent(NATIVE_REACT_CLASS);

export default CPLHappySlider;
