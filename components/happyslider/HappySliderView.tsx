import React from 'react';
import {UIManager, findNodeHandle, Platform} from 'react-native';
import HappySliderNativeWrapper, {
  NATIVE_REACT_CLASS,
} from './HappySliderNativeWrapper';

function dispatchCommand(component: any, command: number, args = []) {
  //
  // NOTE: Below is a fix for an android problem. The native method call requires a string for the command
  // parameter but the typescript environment specifies a number (which is what should be used on iOS).
  //
  // We work around this by typing it as any
  //
  let commandParam: any = command;
  if (Platform.OS === 'android') {
    commandParam = command.toString();
  }
  UIManager.dispatchViewManagerCommand(
    findNodeHandle(component),
    commandParam,
    args,
  );
}

type HappySliderProps = {
  progress: number;
  initialValue: number;
  maxAngle: number;
  padding: number;
  lineWidth: number;
  handleRadius: number;
  lineColor: string;
  handleColor: string;
  steps: number;
  minValue: number;
  maxValue: number;
  snapTime?: number;
  easeType?: number;
  style: React.CSSProperties;
  onSnapTargetSet?: (event: any) => void;
  onTargetValueChanged?: (event: any) => void;
};

class HappySlider extends React.Component {
  public props: HappySliderProps;

  constructor(props: HappySliderProps) {
    super(props);
    this.props = props;
  }
  startSnap() {
    const {startSnap} =
      UIManager.getViewManagerConfig(NATIVE_REACT_CLASS).Commands;
    try {
      dispatchCommand(this, startSnap);
    } catch (error: any) {
      console.error(
        'HappySlider::startSnap() - failed to dispatch command',
        error,
      );
    }
  }
  componentDidMount() {
    const {initialize} =
      UIManager.getViewManagerConfig(NATIVE_REACT_CLASS).Commands;
    try {
      dispatchCommand(this, initialize);
    } catch (error: any) {
      console.error(
        'HappySlider::componentDidMount() - failed to dispatch command',
        error,
      );
      throw error;
    }
  }
  render() {
    return <HappySliderNativeWrapper {...this.props} />;
  }
}

export default HappySlider;
