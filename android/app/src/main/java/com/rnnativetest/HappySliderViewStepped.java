package com.rnnativetest;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;

public class HappySliderViewStepped extends HappySliderView {
    private ValueAnimator snapAnimation;
    private int steps;
    private float minValue;
    private float maxValue;
    private float snapTime;
    private float initialValue;
    private int easeType;
    private TargetValueUpdateSubscriber targetValueUpdateSubscriber;

    public interface TargetValueUpdateSubscriber {
        void targetValueUpdated(float newValue);
    }

    public HappySliderViewStepped(Context c, AttributeSet attrs) {
        super(c, attrs);
        this.minValue = 0;
        this.maxValue = 10;
        this.steps = 11;
        this.snapTime = 0.2f;
    }

    public void setSteps(int steps) {
        this.steps = steps;
        invalidate();
    }

    public int getSteps() {
        return this.steps;
    }

    public void setMinValue(float minValue) {
        this.minValue = minValue;
        invalidate();
    }

    public float getMinValue() {
        return this.minValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
        invalidate();
    }

    public float getMaxValue() {
        return this.maxValue;
    }

    public void setSnapTime(float snapTime) {
        this.snapTime = snapTime;
        invalidate();
    }

    public float getSnapTime() {
        return this.snapTime;
    }

    public float getInitialValue() {
        return this.initialValue;
    }

    public void setInitialValue(float initialValue) {
        this.initialValue = initialValue;
        invalidate();
    }

    public void setEaseType(int easeType) {
        this.easeType = easeType;
    }

    public int getEaseType() {
        return this.easeType;
    }

    public float getCurrentValue() {
        return this.getRange() * this.getProgress() + this.getMinValue();
    }

    public float getTargetValue() {
        return Math.round(this.getProgress() * (this.getSteps() - 1)) + this.getMinValue();
    }

    public TargetValueUpdateSubscriber getTargetValueUpdateSubscriber() {
        return this.targetValueUpdateSubscriber;
    }

    public void setTargetValueUpdateSubscriber(TargetValueUpdateSubscriber targetValueUpdateSubscriber) {
        this.targetValueUpdateSubscriber = targetValueUpdateSubscriber;
    }

    private float getRange() {
        return this.getMaxValue() - this.getMinValue();
    }

    @Override
    public void setProgress(float progress) {
        float oldTargetValue = this.getTargetValue();
        super.setProgress(progress);
        float newTargetValue = this.getTargetValue();
        if (oldTargetValue != newTargetValue && this.getTargetValueUpdateSubscriber() != null) {
            this.getTargetValueUpdateSubscriber().targetValueUpdated(newTargetValue);
        }
    }

    public void startSnap() {
        HappySliderViewStepped view = this;
        if (snapAnimation != null) {
            snapAnimation.cancel();
        }
        float startP = this.getProgress();
        float endP = (this.getTargetValue() - this.getMinValue()) / this.getRange();
        snapAnimation = ValueAnimator.ofFloat(startP, endP);
        snapAnimation.setDuration(Math.round(this.snapTime * 1000));
        snapAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator updatedAnimation) {
                // You can use the animated value in a property that uses the
                // same type as the animation. In this case, you can use the
                // float value in the translationX property.
                float p = (float)updatedAnimation.getAnimatedValue();
                view.setProgress(p);
            }
        });
        snapAnimation.start();
    }

    public void initialize() {
        float p = (this.getInitialValue() - this.getMinValue()) / this.getRange();
        super.setProgress(p);
    }
}
