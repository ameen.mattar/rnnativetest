package com.rnnativetest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class HappySliderView extends View {
    private static final String TAG = "HappySliderView";
    private static final float ALMOST_ZERO = 0.0001f;
    private static final float R2DEG = 180.0f / (float)Math.PI;
    private float mProgress;
    private float mPadding;
    private float mMaxAngle;
    private float mHandleRadius;

    Context context;
    private Canvas mCanvas;
    private Paint mLinePaint;
    private Paint mHandlePaint;
    private float mDensity;

    public HappySliderView(Context c, AttributeSet attrs) {
        super(c, attrs);
        context = c;
        mDensity = context.getResources().getDisplayMetrics().density;
        setupDefaults();
    }

    private void setupDefaults() {
        mMaxAngle = 0.33f * (float)Math.PI;

        mLinePaint = new Paint();
        mLinePaint.setAntiAlias(true);
        mLinePaint.setColor(Color.BLACK);
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setStrokeJoin(Paint.Join.ROUND);
        mLinePaint.setStrokeWidth(4.0f);

        mHandlePaint = new Paint();
        mHandlePaint.setAntiAlias(true);
        mHandlePaint.setColor(Color.BLACK);
        mHandlePaint.setStyle(Paint.Style.STROKE);
        mHandlePaint.setStrokeJoin(Paint.Join.ROUND);
        mHandlePaint.setStrokeWidth(0);
        mHandlePaint.setStyle(Paint.Style.FILL);
        this.setBackgroundColor(Color.TRANSPARENT);

        this.setHandleRadius(20.0f);
        this.setPadding(20.0f);
        this.setLineWidth(4.0f);
    }

    public void setProgress(float p) {
        mProgress = Math.min(Math.max(p, 0), 1);
        invalidate();
    }

    public float getProgress() {
        return mProgress;
    }

    public void setPadding(float padding) {
        mPadding = padding * mDensity;
        invalidate();
    }

    public float getPadding() {
        return mPadding / mDensity;
    }

    public void setMaxAngle(float angle) {
        mMaxAngle = angle;
        invalidate();
    }

    public float getMaxAngle() {
        return mMaxAngle;
    }

    public void setLineWidth(float width) {
        mLinePaint.setStrokeWidth(width * mDensity);
        invalidate();
    }

    public float getLineWidth() {
        return mLinePaint.getStrokeWidth() / mDensity;
    }

    public void setHandleRadius(float radius) {
        mHandleRadius = radius * mDensity;
        invalidate();
    }

    public float getHandleRadius() {
        return mHandleRadius / mDensity;
    }

    public void setLineColor(int rgbIntValue) {
        mLinePaint.setColor(rgbIntValue);
        invalidate();
    }

    public int getLineColor() {
        return mLinePaint.getColor();
    }

    public void setHandleColor(int rgbIntValue) {
        mHandlePaint.setColor(rgbIntValue);
        invalidate();
    }

    public int getHandleColor() {
        return mHandlePaint.getColor();
    }

    private float calcRadius() {
        float hw = getWidth() * 0.5f - mPadding;
        float ca = (float) Math.sin(calcAngle());
        ca = Math.abs(ca) < ALMOST_ZERO ? ALMOST_ZERO : ca;
        return hw / ca;
    }

    private float calcAngle() {
        return mProgress <= 0.5 ?
                mMaxAngle * (1 - 2 * mProgress):
                mMaxAngle * (mProgress * 2 - 1);
    }

    private float calcHandleAngle() {
        return mProgress <= 0.5 ?
                calcAngle() * (mProgress * 2 - 1):
                calcAngle() * (1 - 2 * mProgress);
    }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // your Canvas will draw onto the defined Bitmap
        mCanvas = new Canvas();
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float w = getWidth() - mPadding * 2;
        float h = getHeight() - mPadding * 2;
        float left = mPadding;
        float top = mPadding;
        float cy = top + h * 0.5f;
        float cx = left + w * 0.5f;
        float hr = mHandleRadius;

        // bar
        if (Math.abs(mProgress - 0.5f) <= ALMOST_ZERO) {
            // special case for straight line
            canvas.drawLine(left, cy, left + w, cy, mLinePaint);
            canvas.drawCircle(cx, cy, hr, mHandlePaint);
        } else {
            float a = calcAngle() * R2DEG;
            float r = calcRadius();
            float ha = calcHandleAngle();
            float bh = (float)Math.cos(calcAngle()) * r - r; // height of bounding box of actual bar.
            if (mProgress < 0.5) {
                // sad
                cy += bh * 0.5; // align vertical center of bar to viewport center
                RectF rect = new RectF(cx - r, cy, cx + r, cy + r * 2);
                canvas.drawArc(rect, 270 - a, 2 * a, false, mLinePaint);
                canvas.drawCircle(
                        (float)Math.cos(ha - Math.PI * 0.5) * r + cx,
                        (float)Math.sin(ha - Math.PI * 0.5) * r + r + cy,
                        hr,
                        mHandlePaint);
            } else {
                // happy
                cy -= bh * 0.5; // align vertical center of bar to viewport center
                RectF rect = new RectF(cx - r, cy - r * 2, cx + r, cy);
                canvas.drawArc(rect, 90 - a, 2 * a, false, mLinePaint);
                canvas.drawCircle(
                        (float)Math.cos(Math.PI * 0.5 + ha) * r + cx,
                        (float)Math.sin(Math.PI * 0.5 - ha) * r - r + cy,
                        hr,
                        mHandlePaint);
            }
        }
    }
}
