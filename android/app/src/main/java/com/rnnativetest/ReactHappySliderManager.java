package com.rnnativetest;

import android.graphics.Color;
import android.graphics.Paint;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.facebook.react.util.RNLog;

import java.util.Map;

import javax.annotation.Nullable;

public class ReactHappySliderManager extends SimpleViewManager<HappySliderViewStepped> {

    public static final String REACT_CLASS = "CPLHappySlider";
    public static final int COMMAND_START_SNAP = 1;
    public static final int COMMAND_INITIALIZE = 2;

    ReactApplicationContext mCallerContext;

    public ReactHappySliderManager(ReactApplicationContext reactContext) {
        mCallerContext = reactContext;
    }

    @Override
    public HappySliderViewStepped createViewInstance(ThemedReactContext context) {
        HappySliderViewStepped instance = new HappySliderViewStepped(context, null);
        instance.setTargetValueUpdateSubscriber(newValue -> {
            sendTargetChangedEvent(instance, newValue);
        });
        return instance;
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    // VALUE PROPS

    @ReactProp(name = "initialValue", defaultFloat = (float) 0.0f)
    public void setInitialValue(HappySliderViewStepped view, float value) {
        view.setInitialValue(value);
    }

    @ReactProp(name = "minValue", defaultFloat = (float) 0.0f)
    public void setMinValue(HappySliderViewStepped view, float value) {
        view.setMinValue(value);
    }

    @ReactProp(name = "maxValue", defaultFloat = (float) 0.0f)
    public void setMaxValue(HappySliderViewStepped view, float value) {
        view.setMaxValue(value);
    }

    @ReactProp(name = "steps", defaultInt = (int) 11)
    public void setSteps(HappySliderViewStepped view, int value) {
        view.setSteps(value);
    }

    @ReactProp(name = "snapTime", defaultFloat = (float) 0.2f)
    public void setSnapTime(HappySliderViewStepped view, float value) {
        view.setSnapTime(value);
    }

    @ReactProp(name = "easeType", defaultInt = (int) 1)
    public void setEaseType(HappySliderViewStepped view, int value) {
        view.setEaseType(value);
    }

    // BASIC PROPS

    @ReactProp(name = "progress", defaultFloat = 0.5f)
    public void setProgress(HappySliderViewStepped view, float progress) {
        view.setProgress(progress);
    }

    @ReactProp(name = "maxAngle", defaultFloat = (float) Math.PI * 0.33f)
    public void setMaxAngle(HappySliderView view, float angle) {
        view.setMaxAngle(angle);
    }

    @ReactProp(name = "padding", defaultFloat = 20.0f)
    public void setPadding(HappySliderView view, float padding) {
        view.setPadding(padding);
    }

    @ReactProp(name = "lineWidth", defaultFloat = 2.0f)
    public void setLineWidth(HappySliderView view, float width) {
        view.setLineWidth(width);
    }

    @ReactProp(name = "handleRadius", defaultFloat = 17.0f)
    public void setHandleRadius(HappySliderView view, float radius) {
        view.setHandleRadius(radius);
    }

    @ReactProp(name = "lineColor", defaultInt = Color.BLACK, customType = "Color")
    public void setLineColor(HappySliderView view, int color) {
        view.setLineColor(color);
    }

    @ReactProp(name = "handleColor", defaultInt = Color.BLACK, customType = "Color")
    public void setHandleColor(HappySliderView view, int color) {
        view.setHandleColor(color);
    }

    // EVENTS

    private void sendTargetSetEvent(HappySliderViewStepped view, float targetValue) {
        WritableMap event = Arguments.createMap();
        event.putDouble("targetValue", (double)targetValue);
        ReactContext reactContext = (ReactContext)view.getContext();
        reactContext
                .getJSModule(RCTEventEmitter.class)
                .receiveEvent(view.getId(), "targetValueSet", event);
    }

    private void sendTargetChangedEvent(HappySliderViewStepped view, float targetValue) {
        WritableMap event = Arguments.createMap();
        event.putDouble("targetValue", (double) targetValue);
        ReactContext reactContext = (ReactContext)view.getContext();
        reactContext
                .getJSModule(RCTEventEmitter.class)
                .receiveEvent(view.getId(), "targetValueChanged", event);
    }

    @Override
    public Map getExportedCustomBubblingEventTypeConstants() {
        MapBuilder.Builder<Object, Object> map = MapBuilder.builder().put(
                "targetValueSet",
                MapBuilder.of(
                        "phasedRegistrationNames",
                        MapBuilder.of("bubbled", "onSnapTargetSet")
                ));
        map.put("targetValueChanged",
                MapBuilder.of(
                        "phasedRegistrationNames",
                        MapBuilder.of("bubbled", "onTargetValueChanged")
                )
        );
        return map.build();
    }

    // COMMANDS

    @Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of("startSnap", COMMAND_START_SNAP, "initialize", COMMAND_INITIALIZE);
    }

    @Override
    public void receiveCommand(HappySliderViewStepped view, String commandId, @Nullable ReadableArray args) {
        int commandIdInt = Integer.parseInt(commandId);
        switch (commandIdInt) {
            case COMMAND_START_SNAP:
                view.startSnap();
                sendTargetSetEvent(view, view.getTargetValue());
                break;
            case COMMAND_INITIALIZE:
                view.initialize();
                break;
            default: {
            }
        }
    }
}
