//
//  HappySliderViewStepped.m
//  RnNativeTest
//
//  Created by Ameen Mattar on 7/8/22.
//

#import "HappySliderViewStepped.h"

@interface HappySliderViewStepped () {
  double _snapStartTime;
  double _snapStartValue;
  NSTimer* _animationTimer;
}

@end

@implementation HappySliderViewStepped

@synthesize steps = _steps;
@synthesize minValue = _minValue;
@synthesize maxValue = _maxValue;
@synthesize snapTime = _snapTime;
@synthesize easeType = _easeType;
@synthesize initialValue;
@synthesize targetValueChangeBlock;

-(void)setDefaults
{
  [super setDefaults];
  _minValue = 0;
  _maxValue = 10;
  _steps = _maxValue + 1;
}

-(void)setSteps:(int)steps {
  _steps = steps;
  [self setNeedsDisplay];
}

-(void)setMinValue:(double)minValue {
  _minValue = minValue;
  [self setNeedsDisplay];
}

-(void)setMaxValue:(double)maxValue {
  _maxValue = maxValue;
  [self setNeedsDisplay];
}

-(void)setProgress:(double)progress {
  double oldTargetValue = self.targetValue;
  [super setProgress:progress];
  double newTargetValue = self.targetValue;
  if (oldTargetValue != newTargetValue && self.targetValueChangeBlock != nil) {
    self.targetValueChangeBlock(newTargetValue);
  }
}

-(double)range {
  return self.maxValue - self.minValue;
}

-(double)currentValue {
  return self.minValue + self.range * self.progress;
}

-(double)targetValue {
  return self.minValue + round(self.progress * (self.steps - 1));
}

- (void)onTimerUpdate:(NSTimer *)timer {
  double elapsedTime = CFAbsoluteTimeGetCurrent() - _snapStartTime;
  [self updateWithTime:elapsedTime easeType:_easeType];
}

-(double)startSnap:(BOOL)animate {
  if (_animationTimer != nil) {
    [_animationTimer invalidate];
    _animationTimer = nil;
  }
  if (animate) {
    _snapStartValue = self.currentValue;
    _snapStartTime = CFAbsoluteTimeGetCurrent();
    _animationTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0 / 60.0) target:self selector:@selector(onTimerUpdate:) userInfo:nil repeats:TRUE];
  } else {
    [self updateWithTime:self.snapTime easeType:kLinear];
  }
  return self.targetValue;
}

-(void)endSnap {
  [_animationTimer invalidate];
  _animationTimer = nil;
}

-(void)updateWithTime:(double)elapsedTime easeType:(EaseType)easeType {
  double p = self.snapTime <= 0.0 ? 1.0 : MIN(1.0, elapsedTime / self.snapTime);
  double r = self.targetValue - _snapStartValue;
  switch (self.easeType) {
    case kSqrtOut:
      p = p <= 0 ? 0 : sqrt(p); // cheap easing
      break;
    case kLinear:
    default:
      break;
  }
  self.progress = ((r * p + _snapStartValue) - self.minValue) / self.range;
  if (p == 1) {
    [self endSnap];
  }
}

-(void)initialize {
  [super setProgress:(self.initialValue - self.minValue) / self.range];
}

@end

