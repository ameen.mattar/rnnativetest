//
//  CPLHappySliderManager.m
//  RnNativeTest
//
//  Created by Ameen Mattar on 7/8/22.
//

#import <React/RCTViewManager.h>
#import <React/RCTUIManager.h>
#import <React/RCTLog.h>
#import "HappySliderViewStepped.h"

@interface HappySliderViewStepped (RN)
@property (nonatomic, copy) RCTBubblingEventBlock onSnapTargetSet;
@property (nonatomic, copy) RCTBubblingEventBlock onTargetValueChanged;
-(void)startSnapAnimated;
@end

@implementation HappySliderViewStepped (RN)

-(RCTBubblingEventBlock)onSnapTargetSet {
  RCTBubblingEventBlock block = [self extensionPropertyForKey:@"onSnapTargetSetKey"];
  return block;
}

-(void)setOnSnapTargetSet:(RCTBubblingEventBlock)block {
  [self setExtensionProperty:block forKey:@"onSnapTargetSetKey"];
}

-(RCTBubblingEventBlock)onTargetValueChanged {
  RCTBubblingEventBlock block = [self extensionPropertyForKey:@"onTargetValueChangedKey"];
  return block;
}

-(void)setOnTargetValueChanged:(RCTBubblingEventBlock)block {
  [self setExtensionProperty:block forKey:@"onTargetValueChangedKey"];
}

-(void)startSnapAnimated {
  double targetValue = [self startSnap:true];
  if (self.onSnapTargetSet != nil) {
    self.onSnapTargetSet(@{@"targetValue": @(targetValue)});
  }
}
@end


@interface CPLHappySliderManager : RCTViewManager
@end

@implementation CPLHappySliderManager

RCT_EXPORT_MODULE(CPLHappySlider)
RCT_EXPORT_VIEW_PROPERTY(progress, double)
RCT_EXPORT_VIEW_PROPERTY(maxAngle, double)
RCT_EXPORT_VIEW_PROPERTY(padding, double)
RCT_EXPORT_VIEW_PROPERTY(lineWidth, double)
RCT_EXPORT_VIEW_PROPERTY(handleRadius, double)
RCT_EXPORT_VIEW_PROPERTY(lineColor, UIColor)
RCT_EXPORT_VIEW_PROPERTY(handleColor, UIColor)
RCT_EXPORT_VIEW_PROPERTY(onSnapTargetSet, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onTargetValueChanged, RCTBubblingEventBlock)

RCT_EXPORT_VIEW_PROPERTY(steps, int)
RCT_EXPORT_VIEW_PROPERTY(minValue, double)
RCT_EXPORT_VIEW_PROPERTY(maxValue, double)
RCT_EXPORT_VIEW_PROPERTY(initialValue, double)
RCT_EXPORT_VIEW_PROPERTY(snapTime, double)
RCT_EXPORT_VIEW_PROPERTY(easeType, int)

RCT_EXPORT_METHOD(startSnap:(nonnull NSNumber*)reactTag) {
  [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
    HappySliderViewStepped *view = (HappySliderViewStepped*)viewRegistry[reactTag];
    if (!view || ![view isKindOfClass:[HappySliderViewStepped class]]) {
      RCTLogError(@"Cannot find NativeView with tag #%@", reactTag);
      return;
    }
    [view startSnapAnimated];
  }];
}

RCT_EXPORT_METHOD(endSnap:(nonnull NSNumber*) reactTag) {
  [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
    HappySliderViewStepped *view = (HappySliderViewStepped*)viewRegistry[reactTag];
    if (!view || ![view isKindOfClass:[HappySliderViewStepped class]]) {
      RCTLogError(@"Cannot find NativeView with tag #%@", reactTag);
      return;
    }
    [view endSnap];
  }];
}

RCT_EXPORT_METHOD(initialize:(nonnull NSNumber*) reactTag) {
  [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
    HappySliderViewStepped *view = (HappySliderViewStepped*)viewRegistry[reactTag];
    if (!view || ![view isKindOfClass:[HappySliderViewStepped class]]) {
      RCTLogError(@"Cannot find NativeView with tag #%@", reactTag);
      return;
    }
    [view initialize];
  }];
}

- (UIView *)view
{
  HappySliderViewStepped* instance = [[HappySliderViewStepped alloc] init];
  __weak HappySliderViewStepped* winstance = instance;
  instance.snapTime = 0.2;
  instance.targetValueChangeBlock = ^(double targetValue) {
    if (winstance.onTargetValueChanged != nil) {
      winstance.onTargetValueChanged(@{@"targetValue": @(targetValue)});
    }
  };
  return instance;
}

@end

