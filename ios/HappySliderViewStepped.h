//
//  HappySliderViewStepped.h
//  RnNativeTest
//
//  Created by Ameen Mattar on 7/8/22.
//

#import "HappySliderView.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum tagEaseType : NSUInteger {
    kLinear,
    kSqrtOut
} EaseType;

@interface HappySliderViewStepped : HappySliderView

@property (assign, nonatomic) int steps;
@property (assign, nonatomic) double minValue;
@property (assign, nonatomic) double maxValue;
@property (assign, nonatomic) double initialValue;
@property (readonly, nonatomic) double currentValue;
@property (readonly, nonatomic) double targetValue;
@property (assign, nonatomic) double snapTime;
@property (assign, nonatomic) int easeType;
@property (nonatomic, copy) void (^targetValueChangeBlock)(double targetValue);

-(double)startSnap:(BOOL)animate;
-(void)endSnap;
-(void)initialize;

@end

NS_ASSUME_NONNULL_END

