//
//  HappySliderView.m
//  RnNativeTest
//
//  Created by Ameen Mattar on 7/8/22.
//

#import <Foundation/Foundation.h>
#import "HappySliderView.h"

@interface HappySliderView() {
  double _progress;
  double _maxAngle;
  double _lineWidth;
  double _padding;
  double _handleRadius;
  UIColor* _lineColor;
  UIColor* _handleColor;
  NSMutableDictionary* _extensionProperties;
}
@property (readonly, nonatomic) double angle;
@property (readonly, nonatomic) double handleAngle;
@end

#define H_PI (M_PI * 0.5)
#define ALMOST_ZERO 0.001f

@implementation HappySliderView

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self setDefaults];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
  self = [super initWithCoder:coder];
  if (self) {
    [self setDefaults];
  }
  return self;
}

-(void)setExtensionProperty:(id)value forKey:(NSString*)key {
    [_extensionProperties setValue:value forKey:key];
}

-(id)extensionPropertyForKey:(NSString*)key {
  return [_extensionProperties valueForKey:key];
}

-(void)setDefaults
{
  _maxAngle = (M_PI * 0.33);
  _progress = 0.5f;
  self.lineWidth = 2.0f;
  self.handleRadius = 15.0f;
  self.padding = 20.0f;
  self.lineColor = [UIColor blackColor];
  self.handleColor = [UIColor blackColor];
  _extensionProperties = [[NSMutableDictionary alloc] init];
  [self setOpaque:false];
  [self setBackgroundColor:[UIColor clearColor]];
}

-(void)setProgress:(double)progress {
  _progress = MIN(1, MAX(progress, 0));
  [self setNeedsDisplay];
}

-(double)progress {
  return _progress;
}

-(void)setMaxAngle:(double)maxAngle {
  _maxAngle = maxAngle;
  [self setNeedsDisplay];
}

-(double)maxAngle {
  return _maxAngle;
}

-(double)angle {
  return _progress <= 0.5 ?
  _maxAngle * (_progress * 2 - 1) :
  _maxAngle * (1 - 2 * _progress);
}

-(double)handleAngle {
  return _progress <= 0.5 ?
  (1 - 2 * self.progress) * self.angle:
  self.angle * (_progress * 2 - 1);
}

-(void)setLineWidth:(double)lineWidth {
  _lineWidth = lineWidth;
  [self setNeedsDisplay];
}

-(double)lineWidth {
  return _lineWidth;
}

-(void)setPadding:(double)padding {
  _padding = padding;
  [self setNeedsDisplay];
}

-(double)padding {
  return _padding;
}

- (void)setHandleRadius:(double)handleRadius {
  _handleRadius = handleRadius;
  [self setNeedsDisplay];
}

-(double)handleRadius {
  return _handleRadius;
}

-(void)setLineColor:(UIColor*)lineColor {
  _lineColor = lineColor;
  [self setNeedsDisplay];
}

-(UIColor*)lineColor {
  return _lineColor;
}

-(void)setHandleColor:(UIColor*)handleColor {
  _handleColor = handleColor;
  [self setNeedsDisplay];
}

-(UIColor*)handleColor {
  return _handleColor;
}

-(double)computeRadius {
  double hw = self.frame.size.width * 0.5;
  double ca = sin(self.angle);
  ca = ABS(ca) < ALMOST_ZERO ? ALMOST_ZERO : ca;
  return hw / ca;
}

// NOTE: Some properties are computed and used th

- (void)drawRect:(CGRect)rect {
  double r = [self computeRadius];
  double w = self.frame.size.width;
  double cx = w * 0.5;
  double cy = self.frame.size.height * 0.5;
  double ha = self.handleAngle;
  double hr = self.handleRadius;
  
  CGRect handleRect;
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // Clear background
  CGRect viewRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
  CGContextClearRect(context, viewRect);
    
  double boundingBoxHeight = cos(self.angle) * r - r;
  
  // Setup padding and margins
  CGContextTranslateCTM(context, self.padding, self.padding);
  CGContextScaleCTM(context,
                    (w - self.padding * 2) / w,
                    (w - self.padding * 2) / w);
  
  if (ABS(self.progress - 0.5) <= ALMOST_ZERO) {
    // Special case at 0.5 - straight line
    CGContextMoveToPoint(context, 0, cy);
    CGContextAddLineToPoint(context, w, cy);
    handleRect = CGRectMake(cx - hr, cy - hr, hr * 2, hr * 2);
  } else {
    // Sad and happy - draw an arc, manipulate radius, angle and position to position it right
    if (self.progress < 0.5) {
      CGContextTranslateCTM(context, 0, -boundingBoxHeight * 0.5);
      CGContextTranslateCTM(context, cx, cy - r);
      CGContextRotateCTM(context, H_PI);
    } else {
      CGContextTranslateCTM(context, 0, boundingBoxHeight * 0.5);
      CGContextTranslateCTM(context, cx, r + cy);
      CGContextRotateCTM(context, M_PI + H_PI);
    }
    // Draw the shape
    CGContextAddArc(context, 0, 0, r, self.angle, -self.angle, 0);
    handleRect = CGRectMake(cos(ha) * r - hr, sin(ha) * r - hr, hr * 2, hr * 2);
  }
  // Stroke the added previously
  CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
  CGContextSetLineWidth(context, self.lineWidth);
  CGContextStrokePath(context);
  
  // Add drag handle
  CGContextSetFillColorWithColor(context, self.handleColor.CGColor);
  CGContextAddEllipseInRect(context, handleRect);
  CGContextFillPath(context);
}

@end

