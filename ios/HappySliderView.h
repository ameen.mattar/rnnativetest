//
//  HappySliderView.h
//  RnNativeTest
//
//  Created by Ameen Mattar on 7/8/22.
//

#ifndef HappySliderView_h
#define HappySliderView_h

#import <UIKit/UIKit.h>

@interface HappySliderView : UIView
@property (assign, nonatomic) double progress;
@property (assign, nonatomic) double maxAngle;
@property (assign, nonatomic) double padding;
@property (assign, nonatomic) double lineWidth;
@property (assign, nonatomic) double handleRadius;
@property (assign, nonatomic) UIColor* lineColor;
@property (assign, nonatomic) UIColor* handleColor;
-(void)setExtensionProperty:(id)value forKey:(NSString*)key;
-(id)extensionPropertyForKey:(NSString*)key;
-(void)setDefaults;
@end

#endif /* HappySliderView_h */

