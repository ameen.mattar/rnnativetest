import React, {useRef, useState} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import HappySlider from './HappySlider';

function blendColors(color1: string, color2: string, delta: number): string {
  const a = parseInt(color1.substring(1), 16);
  const b = parseInt(color2.substring(1), 16);
  const da = 1 - delta;
  const db = delta;
  let blended = 0;
  blended =
    (Math.round(((a >> 16) & 255) * da + ((b >> 16) & 255) * db) << 16) |
    (Math.round(((a >> 8) & 255) * da + ((b >> 8) & 255) * db) << 8) |
    Math.round((a & 255) * da + (b & 255) * db);
  let hex = blended.toString(16);
  return '#' + '000000'.substring(hex.length) + hex;
}

function blendPaletteColor(palette: Array<string>, progress: number): string {
  const numColors = palette.length;
  const idxA = Math.min(Math.floor(numColors * progress), numColors - 1);
  const idxB = Math.min(Math.floor(numColors * progress) + 1, numColors - 1);
  const delta = numColors * progress - idxA;
  return blendColors(palette[idxA], palette[idxB], delta);
}
const colors_array_palette = [
  '#B8ABB0',
  '#B7A9C5',
  '#B5A7DA',
  '#A6B6E6',
  '#96C5F3',
  '#87D4FF',
  '#86DDE6',
  '#85E5CE',
  '#83EEB5',
  '#82F69D',
  '#81FF84',
];

const App = () => {
  const [value, setValue] = useState(5);
  const [backgroundColor, setBackgroundColor] = useState(
    blendPaletteColor(colors_array_palette, 0.5),
  );

  return (
    <View style={[styles.container, {backgroundColor: backgroundColor}]}>
      <View style={styles.sliderContainer}>
        <Text style={styles.headerText}>How Happy are you:</Text>
        <HappySlider
          initialValue={value}
          targetValueChange={(val: number) => {
            console.log('targetValueChange: ', val);

            setValue(val);
            setBackgroundColor(
              blendPaletteColor(colors_array_palette, val / 10),
            );
          }}
          targetValueSet={(val: number) => {
            console.log('targetValueSet: ', val);
            setValue(val);
            setBackgroundColor(
              blendPaletteColor(colors_array_palette, val / 10),
            );
          }}
          progressChange={(val: number) => {
            console.log('progressChange: ', val);
          }}
        />
      </View>

      <View style={styles.subContainer}>
        <Text style={styles.subHeader2Text}>{value} / 10</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subContainer: {
    backgroundColor: '#0006',
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    height: 180,
    width: '100%',
  },
  headerText: {
    fontSize: 32,
    fontWeight: 'bold',
    fontStyle: 'italic',
    textAlign: 'center',
  },
  subHeader1Text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
  },
  subHeader2Text: {
    fontSize: 32,
    fontWeight: 'bold',
    fontStyle: 'italic',
    color: '#fff',
    textAlign: 'center',
    marginTop: 10,
  },
  sliderContainer: {
    width: '100%',
    justifyContent: 'center',
  },
});

export default App;
