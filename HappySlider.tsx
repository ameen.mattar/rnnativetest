/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import HappySliderView from './components/happyslider/HappySliderView';
import {useRef, useState} from 'react';
import {Animated, PanResponder, Dimensions} from 'react-native';

type HappySliderProps = {
  initialValue: number;
  disabled?: boolean;
  lineColor?: string;
  handleColor?: string;
  progressChange?: (p: number) => void;
  targetValueChange?: (targetValue: number) => void;
  targetValueSet?: (targetValue: number) => void;
};

const HappySlider = (props: HappySliderProps) => {
  const pan = useRef(new Animated.ValueXY()).current;
  const [progress, setProgress] = useState(0);
  const winWidth = Math.max(100, Dimensions.get('window').width);
  const sliderRef = useRef(null);
  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: (evt, gestureState) => {
        const padding = 20;
        const range = winWidth - padding * 2;
        const p = Math.min(
          Math.max((gestureState.moveX - padding) / range, 0),
          1,
        );
        setProgress(p);
        if (props.progressChange) {
          props.progressChange(p);
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        console.log('onPanResponderRelease');
        if (sliderRef !== null) {
          (sliderRef as any).current.startSnap();
        }
      },
    }),
  ).current;

  return (
    <Animated.View {...panResponder.panHandlers}>
      <HappySliderView
        style={{
          width: '100%',
          height: 150,
        }}
        ref={sliderRef}
        initialValue={props.initialValue}
        progress={progress}
        lineWidth={1}
        padding={32}
        minValue={0}
        maxValue={10}
        steps={11}
        maxAngle={Math.PI * 0.23}
        lineColor={props.lineColor ? props.lineColor : '#000000'}
        handleColor={props.handleColor ? props.handleColor : '#000000'}
        handleRadius={25}
        onSnapTargetSet={(event: any) => {
          if (event.nativeEvent && props.targetValueSet) {
            const {targetValue} = event.nativeEvent;

            props.targetValueSet(targetValue);
          }
        }}
        onTargetValueChanged={(event: any) => {
          if (event.nativeEvent !== null) {
            if (event.nativeEvent && props.targetValueChange) {
              const {targetValue} = event.nativeEvent;
              props.targetValueChange(targetValue);
            }
          }
        }}
      />
    </Animated.View>
  );
};

export default HappySlider;
